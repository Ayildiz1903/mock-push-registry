# Use a base image
FROM ubuntu:20.04

# Set the maintainer label
LABEL maintainer="your-email@example.com"

# Install necessary packages (replace with your requirements)
RUN apt-get update && \
    apt-get install -y \
    curl \
    wget \
    && rm -rf /var/lib/apt/lists/*

# Create a directory for your mock application (replace with your application setup)
WORKDIR /app

# Copy mock application files into the container (replace with your application files)
COPY . /app

# Expose a port if needed (replace with your application's port)
# EXPOSE 8080

# Define the command to run your application (replace with your application command)
# CMD ["./your-application-command"]