node {
    // Define environment variables
    // test
    def IMAGE_NAME = "your-docker-image"
    def IMAGE_TAG = "latest"
    def DOCKER_REGISTRY_CREDENTIALS = 'docker-registry'
    def nameVersion=env.POM_VERSION
    sh "echo ${nameVersion}"
    def zipFileName = "ASSCO-${nameVersion.replaceAll('-parent', '')}.zip"

    // Checkout the code from your repository (assuming Java application code is in 'my-java-app' directory)
    checkout scm

    // Define Docker registry credentials
    withCredentials([usernamePassword(credentialsId: DOCKER_REGISTRY_CREDENTIALS, passwordVariable: 'aRKh6qP5m', usernameVariable: 'testlab-registry')]) {
        withMaven(publisherStrategy: 'EXPLICIT', jdk: 'jdk17', maven: '3.9.3', mavenSettingsFilePath: '/var/jenkins_home/tools/hudson.tasks.Maven_MavenInstallation/3.9.3/conf/settings.xml', mavenLocalRepo: "${WORKSPACE}/.repository") {
        // Build and push Docker image using JIB
 
            // Use Maven to build and JIB to create the Docker image
            //sh "mvn compile jib:build -Djib.to.image=${DOCKER_USERNAME}/${IMAGE_NAME}:${IMAGE_TAG}"
            sh "mvn clean compile jib:build -Djib.allowInsecureRegistries=true"
        }
    }
}